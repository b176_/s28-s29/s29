const express = require("express")
const bodyParser = require("body-parser")

const port = 4000

const application = express()

application.use(bodyParser.urlencoded({ extended: false }));
application.use(bodyParser.json());

application.listen(port, () => console.log(`Express server is running on port ${port}`))

const courses = []

const users = []

application.get('/courses', (request,response) => {
    response.send(courses)
})

application.get('/users', (request,response) => {
    response.send(users)
})

//[SECTION] Create [POST]
//1. Create course
application.post('/course',(req, res) => {
    let newCourse = req.body;
    let courseName = req.body.name;
    let courseInt = req.body.instructor;
    let courseCost = req.body.price;

    if (courseName !== '' && courseInt !== '' && courseCost !== '') {
        courses.push(newCourse);
        res.send(`New ${courseName} Course has been added in our collection`);
    } else {
        res.send('Make sure that Course Name, Instructor, and Price is complete');
    }
});
    
//2. Create user
application.post('/user', (req, res) => {
    let newUser = req.body
    let userFname = req.body.firstName
    let userLname = req.body.lastName
    let userStats = req.body.status
    let userMobil = req.body.mobileNumber

    if (
        userFname !== '' &&
        userLname !== '' &&
        userStats !== '' &&
        userMobil !== '' 
    ){
        users.push(newUser)
        res.send('New user has been created!')    
    }else{
        res.send('Make sure that all fields are not empty.')
    }
});

//[SECTION] Update [PATCH]
application.patch('/change-price', (request, response) => {
    
})

application.patch('/change-status', (request, response) => {
    let userTarget = request.body.lastName
    let newStatus = request.body.status
    let message

    if (users.length > 0) {
        for(let index = 0; index < users.length; index++)
        {
            let lastName = users[index].lastName

            if (userTarget === lastName) {
                users[index].status = newStatus
                message = `Updated status for ${userTarget}`
                break
            } else {
                message = `No match found.`
            }
        }
    } else {
        message = 'users not found'
    }

    response.send(message)
})

//[SECTION] Delete [DELETE]
application.delete('/course', (request, response) => {
    let message
    let targetReference = request.body.name

    if(courses.length > 0){
        for(let index = 0; index < courses.length; index++){
            if(targetReference === courses[index].name){
                courses.splice(index, 1)
                message = `${targetReference} has been deleted.`
                break
            }else{
                message = 'No match found'
            }
        }
    }
    else
    {
        message = 'EMPTY collection.'
    }

    response.send(message)
})

application.delete('/user', (request, response) => {
    let message
    let targetReference = request.body.name

    if (users.length > 0) {
        for(let index = 0; index < users.length; index++){
            if(targetReference === users[index].name){
                users.splice(index, 1)
                message = `${targetReference} has been deleted.`
                break
            }else{
                message = 'No match found'
            }
        }
    } else {
        message = 'EMPTY collection'
    }

    response.send(message)
})