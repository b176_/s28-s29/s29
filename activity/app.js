const express = require("express")

const port = 4000

const application = express()

application.listen(port, () => console.log(`Express server is running on port ${port}`))

application.get('/', (request, response) => {
    response.send("Welcome to Earl's API!")
});

