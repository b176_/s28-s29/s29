const express = require("express")
const bodyParser = require("body-parser")

const port = 4000

const application = express()

application.use(bodyParser.urlencoded({ extended: false }));
application.use(bodyParser.json());

application.listen(port, () => console.log(`Express server is running on port ${port}`))

let tasks = []
let message
let isDuplicate

function duplicateChecker(task_name){
    for(let index = 0; index < tasks.length; index++)
    {
        if(tasks[index].name.toLowerCase() === task_name.toLowerCase())
        {
            message = 'Duplicate entry, task already exists.'
            isDuplicate = true
            break
        }
        else
        {
            isDuplicate = false
        }
    }
}

application.post('/tasks/create', (request, response) => {
    duplicateChecker(request.body.name)

    if(isDuplicate == true)
    {
        response.send(message)
    }
    else
    {
        tasks.push(request.body)
        response.send('New task created!')
    }
})

application.get('/tasks', (request, response) => {
    response.send(tasks)
})

application.delete('/tasks/delete', (request, response) => {
    if(tasks.length > 0)
    {
        for(let index = 0; index < tasks.length; index++)
        {
            if(tasks[index].name.toLowerCase() === request.body.name.toLowerCase())
            {
                tasks.splice(index, 1)
                message = "Task has been deleted!"
                break
            }
            else 
            {
                message = "Task doesn't exist!"
            }
        }

        response.send(message)
    }
    else 
    {
        response.send("No tasks found.")
    }
})

application.patch('/tasks/update-status', (request, response) => {
    if(tasks.length > 0)
    {
        for(let index = 0; index < tasks.length; index++)
        {
            if(tasks[index].name.toLowerCase() === request.body.name.toLowerCase())
            {
                tasks[index].status = request.body.status  
                message = 'Task status has been updated!'
                response.send(message)
                break
            }
        }

        message = "Task doesn't exist!"
        response.send(message)
    }
    else 
    {
        response.send("No tasks found.")
    }
})

application.patch('/tasks/update-name', (request, response) => {
    if(tasks.length > 0)
    {
        for(let index = 0; index < tasks.length; index++)
        {
            if(tasks[index].name === request.body.name)
            {
                duplicateChecker(request.body.new_name)

                if (isDuplicate == false) {
                    tasks[index].name = request.body.new_name
                    message = 'Task name has been updated!'
                    response.send(message)
                }

                break
            }
        }

        message = "Task doesn't exist!"
        response.send(message)
    }
    else 
    {
        response.send("No tasks found.")
    }
})